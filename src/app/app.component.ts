import { Component } from '@angular/core';

@Component({
  selector: 'nav-app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my-nav-app';
}
